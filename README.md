# GitLab Google Cloud Platform Shared Runner Images

Images used by GitLab CI custom executor to run Jobs inside of Google Cloud Platform.

## Provisioning

To provision the images we are using
[chef-solo](https://www.packer.io/docs/provisioners/chef-solo.html). All
the cookbooks are listed inside of the [`cookbooks`](cookbooks) directory.

Some of the cookbooks are vendorded such as `chocolatey`, because
Packer simply uploads the cookbooks and doesn't run anything before the
specified `run_list`, taking a look at
[#6991](https://github.com/hashicorp/packer/issues/69920) and
[#590](https://github.com/hashicorp/packer/issues/590) vendoring the
cookbook is the simplest solution.

The last step of provisioning uses a community
[windows-update](https://github.com/rgl/packer-provisioner-windows-update)
provisioner that will install updates and reboot to finish applying them.

### Sync vendored cookbooks

```
make cookbook-vendor
```

### Container Image for CI Packer Builds

We are creating a custom container image here so that we can use a custom
provisioner for Windows updates. All the image has is the additional
provisioner that is expected to be in the same directory as the packer binary.
